.PHONY: all install uninstall
all: bobControl bob bobMeter

bobControl: bobControl.c
	gcc -Wall -lwiringPi bobControl.c -o bobControl

bob: bob.c
	gcc -Wall bob.c -o bob

bobMeter: bobMeter.c
	gcc -Wall -lasound bobMeter.c -o bobMeter

install: bobControl bob
	[ "`id -u`" = "0" ] || { echo "Must be run as root"; exit 1; }
	cp -f bobControl /usr/local/sbin
	cp -f bob /usr/local/bin
	cp -f init-script /etc/init.d/bobControl
	chmod 755 /etc/init.d/bobControl
	update-rc.d bobControl defaults
	/etc/init.d/bobControl start

uninstall:
	[ "`id -u`" = "0" ] || { echo "Must be run as root"; exit 1; }
	[ -e /etc/init.d/bobControl ] && /etc/init.d/bobControl stop || :
	update-rc.d bobControl remove
	rm -f /dev/bob
	rm -f /usr/local/sbin/bobControl
	rm -f /usr/local/bin/bob
	rm -f /etc/init.d/bobControl

clean:
	rm -f bobControl
	rm -f bob
	rm -f bobMeter
