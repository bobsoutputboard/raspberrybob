#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "config.h"

int readBobDevFile(char *bobBuffer, char* processName){
    int bobFd;
    int length;

    bobFd = open(BOB_DEV_FILE, O_RDONLY);
    if(bobFd < 0){
        fprintf (stderr, "%s: ERROR : Could not open %s !\n", processName, BOB_DEV_FILE) ;
        return -1 ;
    }
    length = read(bobFd,bobBuffer,BOB_BUFFER_SIZE);
    if(length < 0){
        fprintf (stderr, "%s: ERROR : Could not read %s !\n", processName, BOB_DEV_FILE) ;
        return -1 ;
    }
    close(bobFd);

    return length;
}

int writeBobDevFile(char *bobBuffer, int length, char* processName){
    int bobFd;

    bobFd = open(BOB_DEV_FILE, O_WRONLY|O_TRUNC);
    if(bobFd < 0){
        fprintf (stderr, "%s: ERROR : Could not open %s !\n", processName, BOB_DEV_FILE) ;
        return -1;
    }
    length = write(bobFd,bobBuffer,length);
    if(length < 0){
        fprintf (stderr, "%s: ERROR : Could not write %s !\n", processName, BOB_DEV_FILE) ;
        return -1;
    }
    close(bobFd);

    return length;
}


int convertPinNumber(int* pinNumber, const char * pinChar){
    char *endptr;
    (*pinNumber)=(int)strtol(pinChar, &endptr, 10);
    return (endptr==pinChar)? -1 : 0 ;
}


void doRead(int argc, char *argv []){
    int pin;
    char bobBuffer[BOB_BUFFER_SIZE];
    int length;

    //Check for correct number of arguments and convert pin to int
    if((argc!=3)||(convertPinNumber(&pin, argv[2])==-1)){
        fprintf (stderr,"%s : Usage: bob read <outputNumber>\n", argv [0]);
        return;
    }

    //Checking for negative pin number
    if(pin<0){
        fprintf (stderr, "%s: ERROR : pin number cannot be negative \n", argv[0]) ;
        return;
    }

    //Reading BOB_DEV_FILE file
    length = readBobDevFile(bobBuffer,argv[0]);
    if(length<0)return;

    //Checking if output exists
    if(pin>length){
        fprintf (stderr, "%s: ERROR : pin number '%d' does not exists in %s ! Check bobControl configuration. \n", argv[0], pin, BOB_DEV_FILE) ;
        return;
    }

    printf("%c\n",bobBuffer[pin]);
}


void doWrite(int argc, char *argv []){
    int pin;
    char bobBuffer[BOB_BUFFER_SIZE];
    int length;

    //Check for correct number of arguments and convert pin to int
    if((argc!=4)||(convertPinNumber(&pin, argv[2])==-1)){
        fprintf (stderr,"%s : Usage: bob write <outputNumber> <%c/%c>\n", argv [0], BOB_LOW_CHAR, BOB_HIGH_CHAR);
        return;
    }

    //Checking for negative pin number
    if(pin<0){
        fprintf (stderr, "%s: ERROR : pin number cannot be negative \n", argv[0]) ;
        return;
    }

    //Reading BOB_DEV_FILE file
    length = readBobDevFile(bobBuffer,argv[0]);
    if(length<0)return;

    //Checking if output exists
    if(pin>length){
        fprintf (stderr, "%s: ERROR : pin number '%d' does not exists in %s ! Check bobControl configuration. \n", argv[0], pin, BOB_DEV_FILE) ;
        return;
    }

    //Editing buffer
    if(argv[3][0]==BOB_HIGH_CHAR)bobBuffer[pin]=BOB_HIGH_CHAR;
    else if (argv[3][0]==BOB_LOW_CHAR)bobBuffer[pin]=BOB_LOW_CHAR;
    else{
        fprintf (stderr, "%s: ERROR : '%s' is not a valid output value ! Only %c or %c allowed.\n", argv[0], argv[3], BOB_LOW_CHAR, BOB_HIGH_CHAR) ;
        return;
    }

    //Writing new content
    writeBobDevFile(bobBuffer,length,argv[0]);
}


void doToggle(int argc, char *argv []){
    int pin;
    char bobBuffer[BOB_BUFFER_SIZE];
    int length;

    //Check for correct number of arguments and convert pin to int
    if((argc!=3)||(convertPinNumber(&pin, argv[2])==-1)){
        fprintf (stderr,"%s : Usage: bob toggle <outputNumber>\n", argv [0]);
        return;
    }

    //Checking for negative pin number
    if(pin<0){
        fprintf (stderr, "%s: ERROR : pin number cannot be negative \n", argv[0]) ;
        return;
    }

    //Reading BOB_DEV_FILE file
    length = readBobDevFile(bobBuffer,argv[0]);
    if(length<0)return;

    //Checking if output exists
    if(pin>length){
        fprintf (stderr, "%s: ERROR : pin number '%d' does not exists in %s ! Check bobControl configuration. \n", argv[0], pin, BOB_DEV_FILE) ;
        return;
    }

    //Editing buffer
    if(bobBuffer[pin]==BOB_HIGH_CHAR)bobBuffer[pin]=BOB_LOW_CHAR;
    else if(bobBuffer[pin]==BOB_LOW_CHAR)bobBuffer[pin]=BOB_HIGH_CHAR;

    //Writing new content
    writeBobDevFile(bobBuffer,length,argv[0]);
}


void doReadAll(int argc, char *argv []){
    char bobBuffer[BOB_BUFFER_SIZE];
    int length;
    int i;
    int outputNum;

    //Check for correct number of arguments
    if(argc!=2){
        fprintf (stderr,"%s : Usage: bob readall\n", argv [0]);
        return;
    }

    //Reading BOB_DEV_FILE file
    length = readBobDevFile(bobBuffer,argv[0]);
    if(length<0)return;

    //Displaying fancy table
    printf("+-------+-------------------------------+\n");
    printf("| BOARD |            OUTPUTS            |\n");
    printf("|       | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n");
    printf("+-------+---+---+---+---+---+---+---+---+\n");

    for(i=0;i<length;i++){
        outputNum=i%BOB_OUTPUT_PER_BOARD;

        //printing board number
        if(outputNum==0){
            printf("|  %3d  |",i/BOB_OUTPUT_PER_BOARD);
        }

        //printing output value
        printf(" %c ",bobBuffer[i]);

        //printinf column separator
        if(outputNum==BOB_OUTPUT_PER_BOARD-1){
            printf("|\n");
            printf("+-------+---+---+---+---+---+---+---+---+\n");
        }
        else{
            printf(" ");
        }
    }
}


void doWriteAll(int argc, char *argv []){
    int i;
    char bobBuffer[BOB_BUFFER_SIZE];
    int length;
    char charToWrite;

    //Check for correct number of arguments
    if(argc!=3){
        fprintf (stderr,"%s : Usage: bob writeall <%c/%c>\n", argv [0], BOB_LOW_CHAR, BOB_HIGH_CHAR);
        return;
    }

    //Reading BOB_DEV_FILE file
    length = readBobDevFile(bobBuffer,argv[0]);
    if(length<0)return;

    //Preparing the character to write
    if(argv[2][0]==BOB_HIGH_CHAR)charToWrite=BOB_HIGH_CHAR;
    else if (argv[2][0]==BOB_LOW_CHAR)charToWrite=BOB_LOW_CHAR;
    else{
        fprintf (stderr, "%s: ERROR : '%s' is not a valid output value ! Only %c or %c allowed.\n", argv[0], argv[2], BOB_LOW_CHAR, BOB_HIGH_CHAR) ;
        return;
    }

    //filling buffer
    for(i=0;i<length;i++){
        bobBuffer[i]=charToWrite;
    }

    //Writing new content
    writeBobDevFile(bobBuffer,length,argv[0]);
}


void doOutputNumber(int argc, char *argv []){
    int length;
    char bobBuffer[BOB_BUFFER_SIZE];

    //Reading BOB_DEV_FILE file
    length = readBobDevFile(bobBuffer,argv[0]);
    if(length<0)return;

    printf("%d\n",length);
}


void printUsage(char* processName){
    printf ("%s: Usage:bob -h\n"
            "       bob read/write/toggle\n"
            "       bob writeall\n"
            "       bob readall\n"
            "       bob output-number\n", processName) ;
}

int main (int argc, char *argv [])
{

  if ((argc == 1) || (strcasecmp (argv [1], "-h") == 0))
  {
      printUsage(argv[0]);
      return 0;

  }


  else if (strcasecmp (argv [1], "read"   ) == 0) doRead(argc, argv) ;
  else if (strcasecmp (argv [1], "write"  ) == 0) doWrite(argc, argv) ;
  else if (strcasecmp (argv [1], "toggle" ) == 0) doToggle(argc, argv) ;
  else if (strcasecmp (argv [1], "readall"  ) == 0) doReadAll(argc, argv) ;
  else if (strcasecmp (argv [1], "writeall"    ) == 0) doWriteAll(argc, argv) ;
  else if (strcasecmp (argv [1], "output-number"    ) == 0) doOutputNumber(argc, argv);
  else
  {
    fprintf (stderr, "%s: Unknown command: %s.\n", argv [0], argv [1]) ;
    printUsage(argv[0]);
    exit (EXIT_FAILURE) ;
  }
  return 0 ;
}
