#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>
#include <fcntl.h>
#include <getopt.h>
#include <wiringPi.h>
#include "config.h"

#define INOTIFY_EVENT_SIZE      ( sizeof (struct inotify_event) )
#define INOTIFY_EVENT_BUF_LEN   ( 1024 * ( INOTIFY_EVENT_SIZE + 16 ) )

#define DELAY_GPIO_US	10

static struct {
    int bobNumber;
    int gpioG;
    int gpioRCK;
    int gpioSCK;
    int gpioSCLR;
    int gpioD;
} bobParameters = {1,-1,-1,-1,-1,-1};



char* processName;



void setGpioDirection(void){
    digitalWrite(bobParameters.gpioG,1);
    pinMode(bobParameters.gpioG,OUTPUT);
    pinMode(bobParameters.gpioRCK,OUTPUT);
    pinMode(bobParameters.gpioSCK, OUTPUT);
    pinMode(bobParameters.gpioSCLR,OUTPUT);
    pinMode(bobParameters.gpioD,OUTPUT);
}



void resetBob(void){
    digitalWrite(bobParameters.gpioG,1);
    digitalWrite(bobParameters.gpioSCLR,0);
    digitalWrite(bobParameters.gpioRCK, 0);
    usleep(DELAY_GPIO_US);
    digitalWrite(bobParameters.gpioSCLR,1);
    digitalWrite(bobParameters.gpioRCK,1);
    digitalWrite(bobParameters.gpioG,0);
}



void driveBob(char* bobString, int length){
    digitalWrite(bobParameters.gpioRCK,0);
    for(;length>0;length--){
        digitalWrite(bobParameters.gpioSCK,0);
        digitalWrite(bobParameters.gpioD,(bobString[length-1]==BOB_HIGH_CHAR)?1:0);
        usleep(DELAY_GPIO_US);
	digitalWrite(bobParameters.gpioSCK,1);
	usleep(DELAY_GPIO_US);
    }
    digitalWrite(bobParameters.gpioRCK,1);
}



void initDevFile(void){
    int bobFd;
    char bobBuffer[BOB_BUFFER_SIZE];
    int length;
    int i;

    umask(0);
    bobFd = creat(BOB_DEV_FILE, S_IROTH|S_IWOTH|S_IRWXU|S_IRWXG );
    if(bobFd < 0){
        fprintf (stderr, "%s: ERROR : Could not open %s !\n", processName, BOB_DEV_FILE) ;
        exit(1);
    }

    length=(bobParameters).bobNumber*BOB_OUTPUT_PER_BOARD;

    for(i=0;i<length;i++){
        bobBuffer[i]=BOB_LOW_CHAR;
    }

    if(write(bobFd,bobBuffer,length)<0){
        fprintf (stderr, "%s: ERROR : Could not write %s !\n", processName, BOB_DEV_FILE) ;
        exit(1);
    }

    close(bobFd);

}



int cleanUpBinarybBuffer(char* buffer, size_t length){
    int writePos = 0;
    int readPos;
    char currentChar;

    for(readPos = 0 ; readPos < length ; readPos++){
        currentChar = buffer[readPos];
        if( (currentChar == BOB_LOW_CHAR) || (currentChar == BOB_HIGH_CHAR) ){
            buffer[writePos] = currentChar;
            writePos++;
        }
    }
    return writePos;

}



void processBobFileContent(int* stop){
    int targetLength;
    int bobFd;
    int length;
    char bobBuffer[BOB_BUFFER_SIZE+1];

    targetLength = bobParameters.bobNumber*BOB_OUTPUT_PER_BOARD;

    //Reading BOB_DEV_FILE content
    bobFd = open(BOB_DEV_FILE, O_RDONLY);
    if(bobFd < 0){
        fprintf (stderr, "%s: ERROR : Could not open %s !\n", processName, BOB_DEV_FILE) ;
        return;
    }
    length = read(bobFd,bobBuffer,BOB_BUFFER_SIZE);
    if(length < 0){
        fprintf (stderr, "%s: ERROR : Could not read %s !\n", processName, BOB_DEV_FILE) ;
        return;
    }
    close(bobFd);

    //Check for stop instruction
    if((length>=4)&&(bobBuffer[0]=='s')&&(bobBuffer[1]=='t')&&(bobBuffer[2]=='o')&&(bobBuffer[3]=='p')){
        *stop = 1;
        fprintf(stdout,"%s: Stop instruction found\n", processName);
        length=0;       //buffer will be filled with 0 later
    }
    else{
        *stop = 0;
    }

    //Only keep '0' and '1' characters in buffer
    length = cleanUpBinarybBuffer(bobBuffer,length);

    //Matching the specified number of BOB connected by truncating or filling with '0' bobBuffer
    if(length>targetLength)length=targetLength;
    else for(; length<targetLength;length++){
        bobBuffer[length]=BOB_LOW_CHAR;
    }

    //sending command to BOBs
    driveBob(bobBuffer,length);

    //display new bob status
    bobBuffer[length]='\0';
    fprintf(stdout,"%s: New BOB status : %s\n", processName, bobBuffer);

    //Writing new BOB_DEV_FILE content
    bobFd = open(BOB_DEV_FILE, O_WRONLY|O_TRUNC);
    if(bobFd < 0){
        fprintf (stderr, "%s: ERROR : Could not open %s !\n", processName, BOB_DEV_FILE) ;
        return;
    }
    length = write(bobFd,bobBuffer,length);
    if(length < 0){
        fprintf (stderr, "%s: ERROR : Could not write %s !\n", processName, BOB_DEV_FILE) ;
    }
    close(bobFd);
}



void scanDevFileLoop(){
    int i, length;
    int inotifyFd;
    int wd;
    int stop = 0;
    char inotifyBuffer[INOTIFY_EVENT_BUF_LEN];

    //creating the INOTIFY instance
    inotifyFd = inotify_init();
    if ( inotifyFd < 0 ) {
        fprintf (stderr, "%s: ERROR : Could not creat INOTIFY instance !\n", processName) ;
        return;
    }

    //adding inotify watch on BOBFILE
    wd = inotify_add_watch( inotifyFd, BOB_DEV_FILE, IN_CLOSE_WRITE );
    //main loop, checking if file modified
    while(stop == 0){

        //waiting for BOBFILE modification
        length = read(inotifyFd, inotifyBuffer, INOTIFY_EVENT_BUF_LEN );

        for(i=0; i < length; ) {
            struct inotify_event *event = ( struct inotify_event * ) &inotifyBuffer[ i ];
            if(event->mask & IN_CLOSE_WRITE){

                //disabling inotify so we can write the file
                inotify_rm_watch( inotifyFd, wd );

                processBobFileContent(&stop);

                //adding inotify watch on BOBFILE
                wd = inotify_add_watch( inotifyFd, BOB_DEV_FILE, IN_CLOSE_WRITE );

                break;
            }
            i += INOTIFY_EVENT_SIZE + event->len;
        }
    }

    //disabling inotify so we can write the file
    inotify_rm_watch( inotifyFd, wd );

    //closing the INOTIFY instance
    close( inotifyFd );
}


int main(int argc, char *argv [])
{
    int daemonize = 1;
    int everythingOk = 1;
    int checkBobDevFile = 1;

    processName = argv[0];
    //parsing arguments
    while(1){
        int c;
        static struct option long_options[] = {
            { "gpioG",          required_argument,  0,  'G' },
            { "gpioRCK",        required_argument,  0,  'V' },
            { "gpioSCK",        required_argument,  0,  'C' },
            { "gpioSCLR",       required_argument,  0,  'R' },
            { "gpioD",          required_argument,  0,  'D' },
            { "bob-number",     required_argument,  0,  'n' },
            { "no-daemon",      no_argument,        0,  'd' },
            { "no-file-check",  no_argument,        0,  'f' },
            { "help",           no_argument,        0,  'h' },
            { 0, 0, 0, 0 }
        };

        c = getopt_long(argc,argv,"G:V:C:R:D:n:dh",long_options,NULL);
        if( c == -1)        break;
        else if( c == 'G')  bobParameters.gpioG = strtol(optarg,0,10);
        else if( c == 'V')  bobParameters.gpioRCK = strtol(optarg,0,10);
        else if( c == 'C')  bobParameters.gpioSCK = strtol(optarg,0,10);
        else if( c == 'R')  bobParameters.gpioSCLR = strtol(optarg,0,10);
        else if( c == 'D')  bobParameters.gpioD = strtol(optarg,0,10);
        else if( c == 'n')  bobParameters.bobNumber = strtol(optarg,0,10);
        else if( c == 'd')  daemonize=0;
        else if (c == 'f')  checkBobDevFile=0;
        else if( c == 'h'){
            printf("\nUsage: %s <options>\n\n"
                   "Options\n"
                   " --gpioG            -G  defines the gpio pin connected to bob G pin\n"
                   " --gpioRCK          -V  defines the gpio pin connected to bob RCK pin\n"
                   " --gpioSCK          -C  defines the gpio pin connected to bob SCK pin\n"
                   " --gpioSCLR         -R  defines the gpio pin connected to bob SCLR pin\n"
                   " --gpioD            -D  defines the gpio pin connected to bob D pin\n"
                   " --bob-number       -n  specifies the number of bob boards connected,\n"
                   "                        otherwise assume only one board connected\n"
                   " --no-file-check    -f  start event if %s already exists\n"
                   " --debug                prevent the daemonization\n"
                   " --help             -h  show this message\n",
                   processName,BOB_DEV_FILE
                   );
            return 1 ;
        }
    }

    //Checking if all parameters seems correct
    if(bobParameters.gpioG<0){
        fprintf (stderr, "%s: ERROR : Missing or invalid gpioG argument !\n", processName) ;
        everythingOk=0;
    }
    if(bobParameters.gpioRCK<0){
        fprintf (stderr, "%s: ERROR : Missing or invalid gpioRCK argument !\n", processName) ;
        everythingOk=0;
    }
    if(bobParameters.gpioSCK<0){
        fprintf (stderr, "%s: ERROR : Missing or invalid gpioSCK argument !\n", processName) ;
        everythingOk=0;
    }
    if(bobParameters.gpioSCLR<0){
        fprintf (stderr, "%s: ERROR : Missing or invalid gpioSCLR argument !\n", processName) ;
        everythingOk=0;
    }
    if(bobParameters.gpioD<0){
        fprintf (stderr, "%s: ERROR : Missing or invalid gpioD argument !\n", processName) ;
        everythingOk=0;
    }
    if(bobParameters.bobNumber<1){
        fprintf (stderr, "%s: ERROR : Number of connected boards must be greater than 0 !\n", processName) ;
        everythingOk=0;
    }
    else if((bobParameters.bobNumber*BOB_OUTPUT_PER_BOARD) >BOB_BUFFER_SIZE){
        fprintf (stderr, "%s: ERROR : Number of connected boards exceeds buffer capabilities !\n", processName) ;
        everythingOk=0;
    }

    //Checking user privileges
    if (geteuid () != 0){
        fprintf (stderr, "%s: ERROR : Must be root !\n", processName) ;
        everythingOk=0;
    }

    if(checkBobDevFile){
        //Checking if BOB_DEV_FILE already exist
        if(access(BOB_DEV_FILE,F_OK)==0){
            fprintf (stderr, "%s: ERROR : %s file already exist, process might be already running !\n", processName, BOB_DEV_FILE) ;
            everythingOk=0;
        }
    }
    else{
        unlink(BOB_DEV_FILE);
    }

    //if everything is ok, continue, else, abort
    if(everythingOk==1){
        if(daemonize){
            fprintf(stdout,"%s: Starting bob interface in daemon mode\n", processName);
        }
        else{
            fprintf(stdout,"%s: Starting bob interface in debug mode\n", processName);
        }
    }
    else{
        fprintf(stdout,"%s: Something went wrong, use -h to see some help\n", processName);
        return 1;
    }

    //Displaying GPIO config
    fprintf (stdout, "%s: SETTINGS : %d board(s) \n", processName, bobParameters.bobNumber);
    fprintf (stdout, "%s: SETTINGS : Bob G    pin connected to GPIO %d \n", processName, bobParameters.gpioG);
    fprintf (stdout, "%s: SETTINGS : Bob RCK  pin connected to GPIO %d \n", processName, bobParameters.gpioRCK);
    fprintf (stdout, "%s: SETTINGS : Bob SCK  pin connected to GPIO %d \n", processName, bobParameters.gpioSCK);
    fprintf (stdout, "%s: SETTINGS : Bob SCLR pin connected to GPIO %d \n", processName, bobParameters.gpioSCLR);
    fprintf (stdout, "%s: SETTINGS : Bob D    pin connected to GPIO %d \n", processName, bobParameters.gpioD);

    //Turning into daemon
    if(daemonize && daemon(0,0)<0){
        fprintf (stderr, "%s: ERROR : Could not deamonize !\n", processName) ;
        return 1 ;
    }

    //initialyzing GPIOs
    wiringPiSetupGpio();
    setGpioDirection();
    fprintf(stdout,"%s: GPIO initialized\n", processName);

    //resetting BOBs
    resetBob();
    fprintf(stdout,"%s: BOBs reset completed\n", processName);

    //initializing the BOB_DEV_FILE file
    initDevFile();
    fprintf(stdout,"%s: %s initialized\n", processName, BOB_DEV_FILE);

    //main program loop
    fprintf(stdout,"%s: Ready\n", processName);
    scanDevFileLoop();

    //destroying the device file
    unlink(BOB_DEV_FILE);
    fprintf(stdout,"%s: %s removed \n", processName, BOB_DEV_FILE);

    fprintf(stdout,"%s: Exiting\n", processName);
    return(0);
}
