/* 
  From Paul David's tutorial : http://equalarea.com/paul/alsa-audio.html
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <sys/ioctl.h>
#include <alsa/asoundlib.h>
#include "config.h"

static char *device = "plughw:0,0";
static snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
static unsigned int rate = 44100;
static int buffer_frames = 1024;

#define THRESHOLDS_COUNT    4
static uint16_t thresholds[THRESHOLDS_COUNT] = {1024,3328,7424,16168};


int writeBobDevFile(char *bobBuffer, int length){
    int bobFd;

    bobFd = open(BOB_DEV_FILE, O_WRONLY|O_TRUNC);
    if(bobFd < 0){
        fprintf (stderr, "ERROR : Could not open %s !\n", BOB_DEV_FILE) ;
        return -1;
    }
    length = write(bobFd,bobBuffer,length);
    if(length < 0){
        fprintf (stderr, "ERROR : Could not write %s !\n", BOB_DEV_FILE) ;
        return -1;
    }
    close(bobFd);

    return length;
}


snd_pcm_t* initCapture(){
    snd_pcm_t* capture_handle;
    snd_pcm_hw_params_t *hw_params;
    int err;

    if ((err = snd_pcm_open (&capture_handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
        fprintf (stderr, "cannot open audio device %s (%s)\n",
                 device,
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
        fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_any (capture_handle, hw_params)) < 0) {
        fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_set_access (capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
        fprintf (stderr, "cannot set access type (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_set_format (capture_handle, hw_params, format)) < 0) {
        fprintf (stderr, "cannot set sample format (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_set_rate_near (capture_handle, hw_params, &rate, 0)) < 0) {
        fprintf (stderr, "cannot set sample rate (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_set_channels (capture_handle, hw_params, 1)) < 0) {
        fprintf (stderr, "cannot set channel count (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params (capture_handle, hw_params)) < 0) {
        fprintf (stderr, "cannot set parameters (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    snd_pcm_hw_params_free (hw_params);

    if ((err = snd_pcm_prepare (capture_handle)) < 0) {
        fprintf (stderr, "cannot prepare audio interface for use (%s)\n",
                 snd_strerror (err));
        exit (1);
    }

    fprintf(stdout, "Capture interface prepared\n");

    return capture_handle;
}

size_t allocateBuffer(int8_t **buffer){
    size_t buffer_size = buffer_frames * snd_pcm_format_width(format) / 8;

    (*buffer) = malloc(buffer_size);
    memset((*buffer),0x00,buffer_size);

    fprintf(stdout, "buffer allocated, size : %d\n",buffer_size);

    return buffer_size;
}

void displayBarGraph(int16_t value){
    int valuePosition;
    int thresholdsPositions[THRESHOLDS_COUNT];
    int bargraphW;
    int i,j;
    struct winsize w;

    //Get output terminal width
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    bargraphW=w.ws_col-2;

    //Compute thresholds positions
    for(j = 0 ; j < THRESHOLDS_COUNT ; j++){
        thresholdsPositions[j] = bargraphW * thresholds[j] / 32767;
    }

    valuePosition = bargraphW * value / 32767;

    //Draw
    j = 0;
    printf("\r[");
    for(i = 0 ; i < bargraphW ; i++){
        if(i == thresholdsPositions[j] && j < THRESHOLDS_COUNT){
            printf("|");
            j++;
        }
        else
            printf( (i<valuePosition) ? "=" : " ");
    }
    printf("]");
    fflush(stdout);
}


int main (int argc, char *argv[])
{
    int i;
    int16_t peak = 0;
    int err;
    int8_t *buffer;
    size_t buffer_size;
    char bobBuffer[8];

    snd_pcm_t* capture_handle;

    if(argc > 1)device = argv[1];

    capture_handle = initCapture();
    buffer_size = allocateBuffer(&buffer);

    fprintf(stdout,"Let's go !\n");

    for (;;) {
        if ((err = snd_pcm_readi (capture_handle, buffer, buffer_frames)) != buffer_frames) {
            fprintf (stderr, "read from audio interface failed (%s)\n",
                     snd_strerror (err));
            exit (1);
        }

        for(i=0;i<buffer_size;i+=2){
            union{
                int8_t a[2];
                int16_t b;
            } value;
            value.a[0]=buffer[i];
            value.a[1]=buffer[i+1];
            if(value.b>peak) peak=value.b;
        }

        displayBarGraph(peak);

        if(peak>thresholds[3])memcpy(bobBuffer,"11111111",8);
        else if(peak>thresholds[2])memcpy(bobBuffer,"11100111",8);
        else if(peak>thresholds[1])memcpy(bobBuffer,"11000011",8);
        else if(peak>thresholds[0])memcpy(bobBuffer,"10000001",8);
        else memcpy(bobBuffer,"00000000",8);

        writeBobDevFile(bobBuffer,8);

        peak-=(peak/2);
    }

    free(buffer);

    fprintf(stdout, "buffer freed\n");

    snd_pcm_close (capture_handle);
    fprintf(stdout, "audio interface closed\n");

    exit (0);
}
