#ifndef CONFIG_H
#define CONFIG_H

#define BOB_BUFFER_SIZE         256  //maximum supported BOB boards = BOB_BUFFER_SIZE / 8
#define BOB_OUTPUT_PER_BOARD    8
#define BOB_DEV_FILE            "/dev/bob"
#define BOB_HIGH_CHAR           '1'
#define BOB_LOW_CHAR            '0'

#endif
