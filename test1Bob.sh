#!/bin/bash

for (( ; ; ))
do
   echo "10000000" > /dev/bob
   sleep 0.1
   echo "01000000" > /dev/bob
   sleep 0.1
   echo "00100000" > /dev/bob
   sleep 0.1
   echo "00010000" > /dev/bob
   sleep 0.1
   echo "00001000" > /dev/bob
   sleep 0.1
   echo "00000100" > /dev/bob
   sleep 0.1
   echo "00000010" > /dev/bob
   sleep 0.1
   echo "00000001" > /dev/bob
   sleep 0.1
done
